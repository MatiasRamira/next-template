import {
    CLEAR_STATE_USERS,
    CREATE_USER,
    DELETE_USER,
    GET_USERS,
    GET_USER_BY_ID,
    CLEAR_STATE_USER_BY_ID,
    EDIT_USER
} from "../constants/userConstants"

export interface UserState {
    users: any
    user: any
}

interface Action {
    type: string,
    payload?: string
}

export const usersReducer = (state: UserState = { users: [], user: {} }, action: Action = { type: null, payload: null }) => {
    switch (action.type) {
        case GET_USERS:
            return {
                ...state,
                users: action.payload
            }
        case CLEAR_STATE_USERS:
            return {
                ...state,
                users: []
            }
        case GET_USER_BY_ID:
            return {
                ...state,
                user: action.payload
            }
        case CLEAR_STATE_USER_BY_ID:
            return {
                ...state,
                user: {}
            }
        case CREATE_USER:
            return {
                ...state,
            }
        case DELETE_USER:
            return {
                ...state,
            }
        case EDIT_USER:
            return {
                ...state,
            }
        default:
            return state
    }
}
