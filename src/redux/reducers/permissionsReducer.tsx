import {
    GET_PERMISSIONS,
    CLEAR_STATE_PERMISSIONS
} from "../constants/permissionsConstants"

export interface PermissionState {
    permissions: any;
}

interface Action {
    type: string,
    payload?: string
}

export const permissionsReducer = (state: PermissionState = { permissions: [] }, action: Action = { type: null, payload: null }) => {
    switch (action.type) {
        case GET_PERMISSIONS:
            return {
                ...state,
                permissions: action.payload
            }
        case CLEAR_STATE_PERMISSIONS:
            return {
                ...state,
                permissions: []
            }
        default:
            return state
    }
}
