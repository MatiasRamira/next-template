import {
    GET_AUDITS,
    CLEAR_STATE_AUDITS
} from "../constants/auditsConstants"

export interface AuditState {
    audits: any;
}

interface Action {
    type: string,
    payload?: string
}

export const auditsReducer = (state: AuditState = { audits: [] }, action: Action = { type: null, payload: null }) => {
    switch (action.type) {
        case GET_AUDITS:
            return {
                audits: [...state.audits, ...action.payload]
            }
        case CLEAR_STATE_AUDITS:
            return {
                ...state,
                audits: []
            }
        default:
            return state
    }
}
