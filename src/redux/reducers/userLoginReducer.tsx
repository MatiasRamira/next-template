import {
    USER_LOGIN_SUCCESS,
    USER_LOGOUT,
} from "../constants/userConstants"

export interface UserState {
    userInfo: any
}

interface Action {
    type: string,
    payload?: string
}

export const userLoginReducer = (state: UserState = { userInfo: {} }, action: Action = { type: null, payload: null }) => {
    switch (action.type) {
        case USER_LOGIN_SUCCESS:
            return { userInfo: action.payload }
        case USER_LOGOUT:
            return { userInfo: {} }
        default:
            return state
    }
}
