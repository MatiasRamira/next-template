import {
    CLEAR_STATE_ROLES,
    GET_ROLES,
    GET_ROL_BY_ID,
    CLEAR_STATE_ROL_BY_ID,
    CREATE_ROLES,
    EDIT_ROL,
    DELETE_ROL
} from "../constants/rolesConstants"

export interface RolesReducer {
    roles: any;
    rol: any;
}

interface Action {
    type: string,
    payload?: string
}

export const rolesReducer = (state: RolesReducer = { roles: [], rol: {} }, action: Action = { type: null, payload: null }) => {
    switch (action.type) {
        case GET_ROLES:
            return {
                ...state,
                roles: action.payload
            }
        case CLEAR_STATE_ROLES:
            return {
                ...state,
                roles: []
            }
        case GET_ROL_BY_ID:
            return {
                ...state,
                rol: action.payload
            }
        case CLEAR_STATE_ROL_BY_ID:
            return {
                ...state,
                rol: {}
            }
        case CREATE_ROLES:
            return {
                ...state,
            }
        case EDIT_ROL:
            return {
                ...state,
            }
        case DELETE_ROL:
            return {
                ...state,
            }
        default:
            return state
    }
}
