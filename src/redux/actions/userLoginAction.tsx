import {
    USER_LOGIN_SUCCESS,
    USER_LOGOUT,
} from '../constants/userConstants'
import axios from 'axios'
import SERVER from '../server'

export const authUser = (email: string, password: string): any =>
    async (dispatch: any): Promise<void> => {
        try {
            const response = await axios.post(`${SERVER}/api/v1/auth/login`, { email, password })
            if (response.data.data.name) {
                const userData = {
                    name: response.data.data.name,
                    email: response.data.data.email,
                    phone: response.data.data.phone,
                    roles: response.data.data.roles,
                    token: response.data.token
                }
                dispatch({
                    type: USER_LOGIN_SUCCESS,
                    payload: userData,
                })
                localStorage.setItem('userInfo', JSON.stringify(userData))
            }
        } catch (error) {
            return error;
        }
    }

export const logout = (): any => async (dispatch: any) => {
    dispatch({ type: USER_LOGOUT })
    localStorage.removeItem('userInfo')
}
