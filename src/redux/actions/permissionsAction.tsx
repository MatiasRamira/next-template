import {
    GET_PERMISSIONS,
    CLEAR_STATE_PERMISSIONS
} from '../constants/permissionsConstants'
import axios from 'axios'
import SERVER from '../server'

export const getPermissions = (): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            let response = await axios.get(`${SERVER}/api/v1/permissions`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: GET_PERMISSIONS,
                payload: response.data.data
            })

        } catch (error) {
            console.log(error)
        }
    }

export const clearStatePermissions = (): any =>
    async (dispatch: any): Promise<void> => {
        try {
            dispatch({
                type: CLEAR_STATE_PERMISSIONS,
            })
        } catch (error) {
            return error;
        }
    }
