import {
    GET_ROLES,
    CLEAR_STATE_ROLES,
    GET_ROL_BY_ID,
    CLEAR_STATE_ROL_BY_ID,
    CREATE_ROLES,
    EDIT_ROL,
    DELETE_ROL
} from '../constants/rolesConstants'
import axios from 'axios'
import SERVER from '../server'

export const getRoles = (): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            let response = await axios.get(`${SERVER}/api/v1/roles`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: GET_ROLES,
                payload: response.data.data
            })
        } catch (error) {
            console.log(error)
        }
    }

export const clearStateRoles = (): any =>
    async (dispatch: any): Promise<void> => {
        try {
            dispatch({
                type: CLEAR_STATE_ROLES,
            })
        } catch (error) {
            return error;
        }
    }

export const getRolById = (id: string): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            let response = await axios.get(`${SERVER}/api/v1/roles/${id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: GET_ROL_BY_ID,
                payload: response.data.data
            })
        } catch (error) {
            console.log(error)
        }
    }

export const clearStateRolByID = (): any =>
    async (dispatch: any): Promise<void> => {
        try {
            dispatch({
                type: CLEAR_STATE_ROL_BY_ID,
            })
        } catch (error) {
            return error;
        }
    }

export const createRoles = (id: string, name: string, slug: string, description: string, permissionIds: string[], status:string): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            await axios.post(`${SERVER}/api/v1/roles`, {
                id, name, slug, description, permissionIds, status
            }, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: CREATE_ROLES,
            })
        } catch (error) {
            console.log(error)
        }
    }

export const deleteRol = (id: string): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            await axios.delete(`${SERVER}/api/v1/roles/${id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: DELETE_ROL,
            })
        } catch (error) {
            return error
        }
    }

export const editRol = (id: string, name: string, slug: string, description: string, permissionIds: string[], status: string): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            await axios.put(`${SERVER}/api/v1/roles/${id}`, {
                id, name, slug, description, permissionIds, status
            }, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: EDIT_ROL,
            })
        } catch (error) {
            return error
        }
    }
