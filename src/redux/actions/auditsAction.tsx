import {
    GET_AUDITS,
    CLEAR_STATE_AUDITS
} from '../constants/auditsConstants'
import axios from 'axios'
import SERVER from '../server'

export const getAudits = (page:number): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            let response = await axios.get(`${SERVER}/api/v1/audit?page=${page}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: GET_AUDITS,
                payload: response.data.data
            })
        } catch (error) {
            console.log(error)
        }
    }

export const clearStateAudits = (): any =>
    async (dispatch: any): Promise<void> => {
        try {
            dispatch({
                type: CLEAR_STATE_AUDITS,
            })
        } catch (error) {
            return error;
        }
    }
