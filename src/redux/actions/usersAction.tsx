import {
    CLEAR_STATE_USERS,
    CREATE_USER,
    DELETE_USER,
    GET_USERS,
    GET_USER_BY_ID,
    CLEAR_STATE_USER_BY_ID,
    EDIT_USER,
} from '../constants/userConstants'
import axios from 'axios'
import SERVER from '../server'

export const getUsers = (): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            let response = await axios.get(`${SERVER}/api/v1/users`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: GET_USERS,
                payload: response.data.data
            })

        } catch (error) {
            console.log(error)
        }
    }

export const clearStateUsers = (): any =>
    async (dispatch: any): Promise<void> => {
        try {
            dispatch({
                type: CLEAR_STATE_USERS,
            })
        } catch (error) {
            alert("ERROR")
            return error;
        }
    }

export const getUserById = (id: string): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            let response = await axios.get(`${SERVER}/api/v1/users/${id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: GET_USER_BY_ID,
                payload: response.data.data
            })
        } catch (error) {
            console.log(error)
        }
    }

export const clearStateUserByID = (): any =>
    async (dispatch: any): Promise<void> => {
        try {
            dispatch({
                type: CLEAR_STATE_USER_BY_ID,
            })
        } catch (error) {
            return error;
        }
    }

export const createUser = (id: string, email: string, name: string, password: string, phone: string, roleIds: string[]): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            await axios.post(`${SERVER}/api/v1/users`, {
                id, email, name, password, phone, roleIds
            }, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: CREATE_USER,
            })
        } catch (error) {
            return error
        }
    }

export const deleteUser = (id: string): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            await axios.delete(`${SERVER}/api/v1/users/${id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: DELETE_USER,
            })
        } catch (error) {
            return error
        }
    }

export const editUser = (id: string, email: string, name: string, password: string, phone: string, roleIds: string[]): any =>
    async (dispatch: any): Promise<void> => {
        try {
            let userInfo = localStorage.getItem('userInfo')
            let a = userInfo.lastIndexOf(":")
            let b = userInfo.lastIndexOf("}")
            let token = userInfo.slice(a + 2, b - 1)
            await axios.put(`${SERVER}/api/v1/users/${id}`, {
                email, name, password, phone, roleIds
            }, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            dispatch({
                type: EDIT_USER,
            })
        } catch (error) {
            return error
        }
    }
