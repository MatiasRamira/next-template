import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from "redux-thunk"
import { composeWithDevTools } from "@redux-devtools/extension";
import { userLoginReducer } from "../reducers/userLoginReducer"
import { usersReducer } from '../reducers/usersReducer';
import { rolesReducer } from '../reducers/rolesReducer';
import { permissionsReducer } from '../reducers/permissionsReducer';
import { auditsReducer } from '../reducers/auditsReducer';

const reducers = combineReducers({
    userLogin: userLoginReducer,
    users: usersReducer,
    roles: rolesReducer,
    permissions: permissionsReducer,
    audits: auditsReducer,
})

let userInfoFromStorage:any;
if(typeof window !== 'undefined') {
    userInfoFromStorage = localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : undefined
}


const initialState = {
    userLogin: { userInfo: userInfoFromStorage }
} as {}

const middleware = [thunk]

const store = createStore(
    reducers,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware))
)

export type RootState = ReturnType<typeof store.getState>

export default store
