import { useRef, useState } from 'react';
import NextLink from 'next/link';
import {
  Avatar,
  Box,
  Button,
  Divider,
  Hidden,
  Popover,
  Typography
} from '@mui/material';
import { styled } from '@mui/material/styles';
import ExpandMoreTwoToneIcon from '@mui/icons-material/ExpandMoreTwoTone';
import LockOpenTwoToneIcon from '@mui/icons-material/LockOpenTwoTone';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux/store';
import { logout } from '@/redux/actions/userLoginAction';
import { signOut } from 'next-auth/react';

const UserBoxButton = styled(Button)(
  ({ theme }) => `
        padding-left: ${theme.spacing(1)};
        padding-right: ${theme.spacing(1)};
`
);

const MenuUserBox = styled(Box)(
  ({ theme }) => `
        background: ${theme.colors.alpha.black[5]};
        padding: ${theme.spacing(2)};
`
);

const UserBoxText = styled(Box)(
  ({ theme }) => `
        text-align: left;
        padding-left: ${theme.spacing(1)};
`
);

const UserBoxLabel = styled(Typography)(
  ({ theme }) => `
        font-weight: ${theme.typography.fontWeightBold};
        color: ${theme.palette.secondary.main};
        display: block;
`
);

function HeaderUserbox() {
  const dispatch = useDispatch();
  const infoUser = useSelector((state: RootState) => state.userLogin.userInfo);
  let user: { name: any; avatar: any; };
  if (infoUser) {
    if (infoUser.name) {
      user = {
        name: infoUser.name,
        avatar: '/',
      };
    }
  } else {
    user = {
      name: "None",
      avatar: '/',
    };
  }

  const ref = useRef<any>(null);
  const [isOpen, setOpen] = useState<boolean>(false);

  const handleOpen = (): void => {
    setOpen(true);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  const handleClick = async () => {
    dispatch(logout())
    await signOut()
  }

  return (
    <>
      <UserBoxButton color="secondary" ref={ref} onClick={handleOpen}>
        {user ? <Avatar variant="rounded" /* alt={user.name} src={user.avatar} */ /> : null}
        <Hidden mdDown>
          <UserBoxText>
            {user ? <UserBoxLabel variant="body1">{user.name}</UserBoxLabel> : null}
          </UserBoxText>
        </Hidden>
        <Hidden smDown>
          <ExpandMoreTwoToneIcon sx={{ ml: 1 }} />
        </Hidden>
      </UserBoxButton>
      <Popover
        anchorEl={ref.current}
        onClose={handleClose}
        open={isOpen}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
      >
        <MenuUserBox sx={{ minWidth: 210 }} display="flex">
          {user ? <Avatar variant="rounded" /* alt={user.name} src={user.avatar}  *//> : null}
          <UserBoxText>
            {user ? <UserBoxLabel variant="body1">{user.name}</UserBoxLabel> : null}
          </UserBoxText>
        </MenuUserBox>
        <Divider sx={{ mb: 0 }} />
        <Divider />
        <NextLink href="/" passHref>
          <Box sx={{ m: 1 }}>
            <Button color="primary" fullWidth onClick={handleClick}>
              <LockOpenTwoToneIcon sx={{ mr: 1 }} />
              Sign out
            </Button>
          </Box>
        </NextLink>
      </Popover>
    </>
  );
}

export default HeaderUserbox;
