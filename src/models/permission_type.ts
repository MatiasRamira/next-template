export interface PermissionsType {
  id: string;
  name: string;
  slug: string;
  status: string;
}

