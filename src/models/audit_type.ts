export interface AuditType {
  id: string;
  description: string;
  client_ip: string;
  module: string;
  type: string;
  level: string;
  user_id: string;
  user: any;
  created_at: string;
}

