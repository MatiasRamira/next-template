export interface RolesType {
  id: string;
  name: string;
  slug: string;
  description: string;
  status: string;
}

