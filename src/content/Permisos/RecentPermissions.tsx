import { useEffect, useState } from "react"
import { Card, Box, TextField } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { PermissionsType } from 'src/models/permission_type';
import { RootState } from 'src/redux/store';
import RecentPermissionsTable from './RecentPermissionsTable';
import { clearStatePermissions, getPermissions } from "src/redux/actions/permissionsAction";
import CircularProgress from '@mui/material/CircularProgress';

function RecentPermissions() {
  const dispatch = useDispatch();
  const permissionsTypes: PermissionsType[] = useSelector((state: RootState) => state.permissions.permissions);
  const [allPermissions, setAllPermissions] = useState([])

  useEffect(() => {
    dispatch(getPermissions())
    return () => {
      dispatch(clearStatePermissions())
    }
  }, []);

  useEffect(() => {
    setAllPermissions(permissionsTypes)
  }, [permissionsTypes]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAllPermissions(permissionsTypes.filter(p => p.name.includes(e.target.value)))
  }

  return (
    <div>
      {
        permissionsTypes.length > 0 ?
          <Card>
            <TextField
              type="text"
              variant="standard"
              onChange={handleChange}
              sx={{ mb: 2, ml: 2 }}
              label="Busqueda por nombre"
            />
            <RecentPermissionsTable permissionsTypes={allPermissions} />
          </Card>
          :
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <CircularProgress />
          </Box>
      }
    </div>
  );
}

export default RecentPermissions;
