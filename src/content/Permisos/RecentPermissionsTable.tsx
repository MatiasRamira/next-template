import { FC, ChangeEvent, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Divider,
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TableContainer,
  Typography,
} from '@mui/material';
import { PermissionsType } from 'src/models/permission_type';

interface RecentOrdersTableProps {
  className?: string;
  permissionsTypes: PermissionsType[];
}

const applyPagination = (
  permissionsTypes: PermissionsType[],
  page: number,
  limit: number
): PermissionsType[] => {
  return permissionsTypes.slice(page * limit, page * limit + limit);
};

const RecentPermissionsTable: FC<RecentOrdersTableProps> = ({ permissionsTypes }) => {
  const [selectedPermissionsTypes, setSelectedPermissionsTypes] = useState<string[]>([]);
  const [page, setPage] = useState<number>(0);
  const [limit, setLimit] = useState<number>(5);
  const [uuid, setUuid] = useState(false);

  const handlePageChange = (event: any, newPage: number): void => {
    setPage(newPage);
  };

  const handleLimitChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setLimit(parseInt(event.target.value));
    setPage(0);
  };

  const handleClick = () => {
    if (!uuid) {
      setUuid(true);
    } else {
      setUuid(false);
    }
  }

  const paginatedPermissionsTypes = applyPagination(
    permissionsTypes,
    page,
    limit,
  );

  return (
    <Card>
      <Divider />
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombre</TableCell>
              <TableCell>Slug</TableCell>
              {/* <TableCell>Estado</TableCell> */}
              {/* <TableCell align="right">Acciones</TableCell> */}
            </TableRow>
          </TableHead>
          <TableBody>
            {paginatedPermissionsTypes.map((permissionsTypes) => {
              const isPermissionsTypesSelected = selectedPermissionsTypes.includes(
                permissionsTypes.id
              );
              return (
                <TableRow
                  hover
                  key={permissionsTypes.id}
                  selected={isPermissionsTypesSelected}
                >
                  <TableCell onClick={handleClick} sx={{ cursor: "pointer" }}>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {uuid ? `${permissionsTypes.id}` : `${permissionsTypes.id.slice(0, 4)}...`}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {permissionsTypes.name}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {permissionsTypes.slug}
                    </Typography>
                  </TableCell>
                  {/* <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {permissionsTypes.status}
                    </Typography>
                  </TableCell> */}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Box p={2}>
        <TablePagination
          component="div"
          count={permissionsTypes.length}
          onPageChange={handlePageChange}
          onRowsPerPageChange={handleLimitChange}
          page={page}
          rowsPerPage={limit}
          rowsPerPageOptions={[5, 10]}
        />
      </Box>
    </Card>
  );
};

RecentPermissionsTable.propTypes = {
  permissionsTypes: PropTypes.array.isRequired
};

RecentPermissionsTable.defaultProps = {
  permissionsTypes: []
};

export default RecentPermissionsTable;
