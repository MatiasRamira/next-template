import { FC, ChangeEvent, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Divider,
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TableContainer,
  Typography,
  useTheme,
} from '@mui/material';
import { AuditType } from 'src/models/audit_type';
import { useDispatch } from 'react-redux';
import { getAudits } from 'src/redux/actions/auditsAction';
import CircularProgress from '@mui/material/CircularProgress';

interface RecentAuditTableProps {
  className?: string;
  auditTypes: AuditType[];
}

const applyPagination = (
  auditTypes: AuditType[],
  page: number,
  limit: number
): AuditType[] => {
  return auditTypes.slice(page * limit, page * limit + limit);
};

const RecentAuditTable: FC<RecentAuditTableProps> = ({ auditTypes }) => {
  const [selectedAuditTypes, setSelectedAuditTypes] = useState<string[]>([]);
  const [page, setPage] = useState<number>(0);
  const [limit, setLimit] = useState<number>(5);
  const [uuid, setUuid] = useState(false);
  const [count, setCount] = useState<number>(2);
  const [beforePage, setBeforePage] = useState<number>(0);
  const [charge, setCharge] = useState(false);
  const dispatch = useDispatch()

  useEffect(() => {
    if (auditTypes.length === (page * limit + limit)) {
      if (page !== beforePage) {
        setBeforePage(page)
        setCount(count + 1)
        setCharge(true)
        dispatch(getAudits(count))
      }
    }
  }, [page, limit]);

  useEffect(() => {
    setCharge(false)
  }, [auditTypes.length]);

  const handlePageChange = (event: any, newPage: number): void => {
    setPage(newPage);
  };

  const handleLimitChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setLimit(parseInt(event.target.value));
    setPage(0);
  };

  const handleClick = () => {
    if (!uuid) {
      setUuid(true);
    } else {
      setUuid(false);
    }
  }

  const paginatedAuditTypes = applyPagination(
    auditTypes,
    page,
    limit,
  );

  return (
    <Card>
      <Divider />
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Fecha</TableCell>
              <TableCell>IP</TableCell>
              <TableCell>Descripción</TableCell>
              <TableCell>Modulo</TableCell>
              <TableCell>Tipo</TableCell>
              <TableCell>Nivel</TableCell>
              <TableCell>ID Usuario</TableCell>
              <TableCell>Usuario</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {paginatedAuditTypes.map((auditTypes) => {
              const isAuditTypesSelected = selectedAuditTypes.includes(
                auditTypes.id
              );
              return (
                <TableRow
                  hover
                  key={auditTypes.id}
                  selected={isAuditTypesSelected}
                >
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {auditTypes.created_at.slice(0, 10)} {auditTypes.created_at.slice(11, 21)}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {auditTypes.client_ip}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {auditTypes.description}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {auditTypes.module}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {auditTypes.type}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {auditTypes.level}
                    </Typography>
                  </TableCell>
                  <TableCell onClick={handleClick} sx={{ cursor: "pointer" }}>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {uuid ? `${auditTypes.user_id ? auditTypes.user_id : "null"}` : `${auditTypes.user_id ? `${auditTypes.user_id.slice(0, 4)}...` : "null"}`}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {auditTypes.user ? auditTypes.user.name : "null"}
                    </Typography>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Box p={2} sx={{display:"flex", justifyContent:"flex-end"}}>
        {charge ? <CircularProgress sx={{mr:"30px"}}></CircularProgress> : null }
        <TablePagination
          component="div"
          count={auditTypes.length}
          onPageChange={handlePageChange}
          onRowsPerPageChange={handleLimitChange}
          page={page}
          rowsPerPage={limit}
          rowsPerPageOptions={[5, 10]}
        />
      </Box>

    </Card>
  );
};

RecentAuditTable.propTypes = {
  auditTypes: PropTypes.array.isRequired
};

RecentAuditTable.defaultProps = {
  auditTypes: []
};

export default RecentAuditTable;
