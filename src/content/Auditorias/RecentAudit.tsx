import { useEffect, useState } from "react"
import { Card, Box, TextField } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { AuditType } from 'src/models/audit_type';
import { RootState } from 'src/redux/store';
import RecentAuditTable from './RecentAuditTable';
import { clearStateAudits, getAudits } from "src/redux/actions/auditsAction";
import CircularProgress from '@mui/material/CircularProgress';

function RecentAudit() {
  const dispatch = useDispatch();
  const auditTypes: AuditType[] = useSelector((state: RootState) => state.audits.audits);
  const [allAudits, setAllAudits] = useState([]);

  useEffect(() => {
    dispatch(getAudits(1))
    return () => {
      dispatch(clearStateAudits())
    }
  }, []);

  useEffect(() => {
    setAllAudits(auditTypes)
  }, [auditTypes]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAllAudits(auditTypes.filter(a => a.client_ip.includes(e.target.value)))
  }

  return (
    <div>
      {
        auditTypes.length > 0 ?
          <Card>
            <TextField
              type="text"
              variant="standard"
              onChange={handleChange}
              sx={{ mb: 2, ml: 2 }}
              label="Busqueda por IP"
            />
            <RecentAuditTable auditTypes={allAudits} />
          </Card>
          :
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <CircularProgress />
          </Box>
      }
    </div>
  );
}

export default RecentAudit;