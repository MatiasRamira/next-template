import { RootState } from '@/redux/store';
import { Typography, Grid } from '@mui/material';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

function PageHeader() {
  const userInfo = useSelector((state: RootState) => state.userLogin.userInfo);
  const [ user, setUser ] = useState({
    name: "",
    roles: []
  })
  useEffect(() => setUser(userInfo), [userInfo])

  return (
    <Grid container justifyContent="space-between" alignItems="center">
      <Grid item>
        <Typography variant="h3" component="h3" gutterBottom>
          Inicio
        </Typography>
      </Grid>
      <Grid item>
        Bienvenido { !user.name ? "usuario" : user.name }
      </Grid>
    </Grid>
  );
}

export default PageHeader;
