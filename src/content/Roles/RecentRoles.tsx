import { useEffect, useState } from "react"
import { Card, Box, TextField } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { RolesType } from 'src/models/rol_type';
import { RootState } from 'src/redux/store';
import RecentRolesTable from './RecentRolesTable';
import { clearStateRoles, getRoles } from "src/redux/actions/rolesAction";
import CircularProgress from '@mui/material/CircularProgress';

function RecentRoles() {
  const dispatch = useDispatch();
  const rolesTypes: RolesType[] = useSelector((state: RootState) => state.roles.roles);
  const [allRoles, setAllRoles] = useState([]);

  useEffect(() => {
    dispatch(getRoles())
    return () => {
      dispatch(clearStateRoles())
    }
  }, []);

  useEffect(() => {
    setAllRoles(rolesTypes)
  }, [rolesTypes]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAllRoles(rolesTypes.filter(r => r.name.includes(e.target.value)))
  }

  return (
    <div>
      {
        rolesTypes.length > 0 ?
          <Card>
            <TextField
              type="text"
              variant="standard"
              onChange={handleChange}
              sx={{ mb: 2, ml: 2 }}
              label="Busqueda por nombre"
            />
            <RecentRolesTable rolesTypes={allRoles} />
          </Card>
          :
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <CircularProgress />
          </Box>
      }
    </div>
  );
}

export default RecentRoles;
