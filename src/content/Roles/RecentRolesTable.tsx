import { FC, ChangeEvent, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Tooltip,
  Divider,
  Box,
  Card,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TableContainer,
  Typography,
  useTheme,
} from '@mui/material';
import { RolesType } from 'src/models/rol_type';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import DeleteTwoToneIcon from '@mui/icons-material/DeleteTwoTone';
import { deleteRol, getRoles } from 'src/redux/actions/rolesAction';
import { useDispatch } from 'react-redux';
import NextLink from 'next/link';

interface RecentRolesTableProps {
  className?: string;
  rolesTypes: RolesType[];
}

const applyPagination = (
  rolesTypes: RolesType[],
  page: number,
  limit: number
): RolesType[] => {
  return rolesTypes.slice(page * limit, page * limit + limit);
};

const RecentRolesTable: FC<RecentRolesTableProps> = ({ rolesTypes }) => {
  const [selectedRolesTypes, setSelectedRolesTypes] = useState<string[]>([]);
  const [page, setPage] = useState<number>(0);
  const [limit, setLimit] = useState<number>(5);
  const [uuid, setUuid] = useState(false);
  const dispatch = useDispatch();

  const handlePageChange = (event: any, newPage: number): void => {
    setPage(newPage);
  };

  const handleLimitChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setLimit(parseInt(event.target.value));
    setPage(0);
  };

  const handleDelete = (id: string) => {
    if (id === "0ce7ced4-d2d5-4072-b2db-bd8c1f58d75b") {
      alert("No se puede eliminar el root")
    } else {
      dispatch(deleteRol(id))
      alert("Rol eliminado")
      dispatch(getRoles())
    }
  }

  const handleClick = () => {
    if (!uuid) {
      setUuid(true);
    } else {
      setUuid(false);
    }
  }

  const paginatedRolesTypes = applyPagination(
    rolesTypes,
    page,
    limit,
  );
  const theme = useTheme();

  return (
    <Card>
      <Divider />
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombre</TableCell>
              <TableCell>Slug</TableCell>
              <TableCell>Estado</TableCell>
              <TableCell align="right">Acciones</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {paginatedRolesTypes.map((rolesTypes) => {
              const isRolesTypesSelected = selectedRolesTypes.includes(
                rolesTypes.id
              );
              return (
                <TableRow
                  hover
                  key={rolesTypes.id}
                  selected={isRolesTypesSelected}
                >
                  <TableCell onClick={handleClick} sx={{ cursor: "pointer" }}>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {uuid ? `${rolesTypes.id}` : `${rolesTypes.id.slice(0, 4)}...`}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {rolesTypes.name}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {rolesTypes.slug}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {rolesTypes.status}
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                  <NextLink href={`/roles/editar/${rolesTypes.id}`} passHref>
                      <Tooltip title="Editar rol" arrow>
                        <IconButton
                          sx={{
                            '&:hover': {
                              background: theme.colors.primary.lighter
                            },
                            color: theme.palette.primary.main
                          }}
                          color="inherit"
                          size="small"
                        >
                          <EditTwoToneIcon fontSize="small" />
                        </IconButton>
                      </Tooltip>
                      </NextLink>
                    {rolesTypes.id !== "0ce7ced4-d2d5-4072-b2db-bd8c1f58d75b" ?
                      <Tooltip title="Borrar rol" arrow>
                        <IconButton
                          onClick={() => handleDelete(rolesTypes.id)}
                          sx={{
                            '&:hover': { background: theme.colors.error.lighter },
                            color: theme.palette.error.main
                          }}
                          color="inherit"
                          size="small"
                        >
                          <DeleteTwoToneIcon fontSize="small" />
                        </IconButton>
                      </Tooltip>
                      : null
                    }
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Box p={2}>
        <TablePagination
          component="div"
          count={rolesTypes.length}
          onPageChange={handlePageChange}
          onRowsPerPageChange={handleLimitChange}
          page={page}
          rowsPerPage={limit}
          rowsPerPageOptions={[5, 10]}
        />
      </Box>
    </Card>
  );
};

RecentRolesTable.propTypes = {
  rolesTypes: PropTypes.array.isRequired
};

RecentRolesTable.defaultProps = {
  rolesTypes: []
};

export default RecentRolesTable;
