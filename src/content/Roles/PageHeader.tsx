import { Typography, Button, Grid } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import NextLink from 'next/link';

function PageHeader() {

  return (
    <Grid container justifyContent="space-between" alignItems="center">
      <Grid item>
        <Typography variant="h3" component="h3" gutterBottom>
          Roles
        </Typography>
      </Grid>
      <Grid item>
      <NextLink href="/roles/crear" passHref>
        <Button
          sx={{ mt: { xs: 2, md: 0 } }}
          variant="contained"
          startIcon={<AddIcon fontSize="small" />}
        >
          Crear rol
        </Button>
        </NextLink>
      </Grid>
    </Grid>
  );
}

export default PageHeader;
