import { useEffect, useState } from "react"
import { Card, Box, TextField } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { UserType } from 'src/models/user_type';
import { clearStateUsers, getUsers } from 'src/redux/actions/usersAction';
import { RootState } from 'src/redux/store';
import RecentUsersTable from "./RecentUsersTable";
import CircularProgress from '@mui/material/CircularProgress';

function RecentUsers() {
  const dispatch = useDispatch();
  const userTypes: UserType[] = useSelector((state: RootState) => state.users.users);
  const [allUsers, setAllUsers] = useState([]);

  useEffect(() => {
    dispatch(getUsers())
    return () => {
      dispatch(clearStateUsers())
    }
  }, []);

  useEffect(() => {
    setAllUsers(userTypes)
  }, [userTypes]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAllUsers(userTypes.filter(u => u.name.includes(e.target.value)))
  }

  return (
    <div>
      {
        userTypes.length > 0 ?
          <Card>
            <TextField
              type="text"
              variant="standard"
              onChange={handleChange}
              sx={{ mb: 2, ml: 2 }}
              label="Busqueda por nombre"
            />
            <RecentUsersTable userTypes={allUsers} />
          </Card>
          :
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <CircularProgress />
          </Box>
      }
    </div>
  );
}

export default RecentUsers;
