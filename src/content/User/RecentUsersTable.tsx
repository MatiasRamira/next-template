import { FC, ChangeEvent, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Tooltip,
  Divider,
  Box,
  Card,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TableContainer,
  Typography,
  useTheme,
} from '@mui/material';
import { UserType } from 'src/models/user_type';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import DeleteTwoToneIcon from '@mui/icons-material/DeleteTwoTone';
// import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { deleteUser, getUsers } from 'src/redux/actions/usersAction';
import { RootState } from 'src/redux/store';
import NextLink from 'next/link';

interface RecentUsersTableProps {
  className?: string;
  userTypes: UserType[];
}

const applyPagination = (
  userTypes: UserType[],
  page: number,
  limit: number
): UserType[] => {
  return userTypes.slice(page * limit, page * limit + limit);
};

const RecentUsersTable: FC<RecentUsersTableProps> = ({ userTypes }) => {
  const [selectedUserTypes, setSelectedUserTypes] = useState<string[]>([]);
  const [page, setPage] = useState<number>(0);
  const [limit, setLimit] = useState<number>(5);
  const [uuid, setUuid] = useState(false);
  const dispatch = useDispatch();
  const userLogin = useSelector((state: RootState) => state.userLogin);
  const { userInfo } = userLogin;

  const handlePageChange = (event: any, newPage: number): void => {
    setPage(newPage);
  };

  const handleLimitChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setLimit(parseInt(event.target.value));
    setPage(0);
  };

  const handleDelete = (id: string) => {
    if (id === "04258fa2-eab6-4616-a44e-6459eaf9db75") {
      alert("No se puede eliminar el root")
    } else {
      dispatch(deleteUser(id))
      alert("User delete")
      dispatch(getUsers())
    }
  }

  const handleClick = () => {
    if (!uuid) {
      setUuid(true);
    } else {
      setUuid(false);
    }
  }

  const paginatedUserTypes = applyPagination(
    userTypes,
    page,
    limit,
  );
  const theme = useTheme();

  return (
    <Card>
      <Divider />
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombre</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Teléfono</TableCell>
              <TableCell align="right">Acciones</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {paginatedUserTypes.map((userTypes) => {
              const isUserTypesSelected = selectedUserTypes.includes(
                userTypes.id
              );
              return (
                <TableRow
                  hover
                  key={userTypes.id}
                  selected={isUserTypesSelected}
                >
                  <TableCell onClick={handleClick} sx={{ cursor: "pointer" }}>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {uuid ? `${userTypes.id}` : `${userTypes.id.slice(0, 4)}...`}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {userTypes.name}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {userTypes.email}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {userTypes.phone}
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    <NextLink href={`/usuarios/editar/${userTypes.id}`} passHref>
                      <Tooltip title="Editar usuario" arrow>
                        <IconButton
                          sx={{
                            '&:hover': {
                              background: theme.colors.primary.lighter
                            },
                            color: theme.palette.primary.main
                          }}
                          color="inherit"
                          size="small"
                        >
                          <EditTwoToneIcon fontSize="small" />
                        </IconButton>
                      </Tooltip>
                    </NextLink>
                    {userInfo.name !== userTypes.name ?
                      <Tooltip title="Borrar usuario" arrow>
                        <IconButton
                          onClick={() => handleDelete(userTypes.id)}
                          sx={{
                            '&:hover': { background: theme.colors.error.lighter },
                            color: theme.palette.error.main
                          }}
                          color="inherit"
                          size="small"
                        >
                          <DeleteTwoToneIcon fontSize="small" />
                        </IconButton>
                      </Tooltip>
                      : null
                    }
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>

        </Table>
      </TableContainer>
      <Box p={2}>
        <TablePagination
          component="div"
          count={userTypes.length}
          onPageChange={handlePageChange}
          onRowsPerPageChange={handleLimitChange}
          page={page}
          rowsPerPage={limit}
          rowsPerPageOptions={[5, 10]}
        />
      </Box>
    </Card>
  );
};

RecentUsersTable.propTypes = {
  userTypes: PropTypes.array.isRequired
};

RecentUsersTable.defaultProps = {
  userTypes: []
};

export default RecentUsersTable;
