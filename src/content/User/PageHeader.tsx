import { Typography, Button, Grid } from '@mui/material';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import NextLink from 'next/link';

function PageHeader() {
  return (
    <Grid container justifyContent="space-between" alignItems="center">
      <Grid item>
        <Typography variant="h3" component="h3" gutterBottom>
          Usuarios
        </Typography>
      </Grid>
      <Grid item>
      <NextLink href="/usuarios/crear" passHref>
        <Button
          sx={{ mt: { xs: 2, md: 0 } }}
          variant="contained"
          startIcon={<PersonAddIcon fontSize="small" />}
        >
          Crear usuario
        </Button>
        </NextLink>
      </Grid>
    </Grid>
  );
}

export default PageHeader;
