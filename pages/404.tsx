import {
  Box,
  Typography,
  Container,
  Button,
  styled
} from '@mui/material';
import Head from 'next/head';

const MainContent = styled(Box)(
  ({ }) => `
    height: 100%;
    display: flex;
    flex: 1;
    overflow: auto;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`
);
function Status404() {
  return (
    <>
      <Head>
        <title>Status - 404</title>
      </Head>
      <MainContent>
        <Container maxWidth="md">
          <Box textAlign="center">
            <Typography variant="h2" sx={{ my: 2 }}>
              La página que estabas buscando no existe.
            </Typography>
          </Box>
          <Container maxWidth="sm">
            <Box sx={{ textAlign: 'center', mt: 3, p: 4 }}>
              <Button href="/" variant="outlined">
                Volver al inicio
              </Button>
            </Box>
          </Container>
        </Container>
      </MainContent>
    </>
  );
}

export default Status404;