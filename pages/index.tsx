import React, { Fragment, useState, FormEventHandler, useEffect } from "react";
import {
  Container,
  Button,
  Grid,
  Paper,
  Box,
  Typography,
  TextField,
} from "@mui/material";
import Logo from "../src/components/LogoSign"
import { useDispatch } from "react-redux";
import { authUser } from "src/redux/actions/userLoginAction";
import CircularProgress from '@mui/material/CircularProgress';
import { useRouter } from 'next/router'
import { signIn, useSession } from "next-auth/react";

type LoginType = {
  email: string;
  password: string;
};

function index() {
  const [loginData, setLoginData] = useState<LoginType>({
    email: "",
    password: "",
  });
  const [charge, setCharge] = useState(false);
  const router = useRouter()
  const dispatch = useDispatch();
  const { data: session } = useSession();

  useEffect(() => {
    if(session) {
      router.push("/home")
    }
  },[session])

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setLoginData({ ...loginData, [e.target.name]: e.target.value });
  };

  const handleSubmit: FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();
    setCharge(true)
    if (loginData.password.length < 6) {
      alert("La contraseña debe tener más de 6 caracteres.")
      setCharge(false)
    } else {
      dispatch(authUser(loginData.email, loginData.password))
        .then(async r => {
          if (r === undefined) {
            setCharge(false);
            await signIn("credentials", {
              redirect: false,
            });
          } else {
            if (r && r.name === "AxiosError") {
              alert("Datos inválidos.")
              setCharge(false);
            }
          }
        })
    }
  };

  return (
    <Fragment>
      <div style={{
        backgroundImage: `url(https://fondosmil.com/fondo/17553.jpg)`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center center",
        backgroundAttachment: "fixed",
        height: "100vh"
      }}>
        <div style={{ marginLeft: -30 }}>
          <Logo></Logo>
        </div>
        <Container maxWidth="sm">
          <Grid
            container
            direction="column"
            alignItems="center"
            justifyContent="center"
            sx={{ minHeight: "80vh" }}
          >
            <Grid item>
              <Paper sx={{ padding: "1.2em", borderRadius: "0.5em", boxShadow: "box-shadow: rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px;" }}>
                <Typography sx={{ mt: 1, mb: 1, textAlign: "center" }} variant="h3">
                  Iniciar sesión
                </Typography>
                <Box component="form" onSubmit={handleSubmit}>
                  <TextField
                    name="email"
                    margin="normal"
                    type="text"
                    fullWidth
                    label="Email o usuario"
                    sx={{ mt: 2, mb: 1.5 }}
                    required
                    onChange={handleChange}
                  ></TextField>
                  <TextField
                    name="password"
                    margin="normal"
                    type="password"
                    autoComplete="on"
                    fullWidth
                    label="Password"
                    sx={{ mt: 1.5, mb: 1.5 }}
                    required
                    onChange={handleChange}
                  ></TextField>
                  <Button
                    fullWidth
                    type="submit"
                    variant="contained"
                    sx={{ mt: 1.5, mb: 3 }}
                  >
                    Iniciar sesion
                  </Button>
                </Box>
                <Box sx={{ display: 'flex', justifyContent: "center" }}>
                  {charge ? <CircularProgress /> : null}
                </Box>
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </div>
    </Fragment >
  );
}


export default index;

