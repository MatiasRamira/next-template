import Head from 'next/head';
import SidebarLayout from '@/layouts/SidebarLayout';
import PageTitleWrapper from '@/components/PageTitleWrapper';
import Footer from '@/components/Footer';
import { useState, useEffect } from 'react';
import {
    Container,
    Grid,
    Card,
    CardHeader,
    CardContent,
    Divider,
    Typography,
    Button,
    Select,
    CircularProgress
} from '@mui/material';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Cancel';
import { useDispatch, useSelector } from 'react-redux';
import { createUser } from 'src/redux/actions/usersAction';
import { v4 as uuidv4 } from 'uuid';
import { clearStateRoles, getRoles } from 'src/redux/actions/rolesAction';
import { RootState } from 'src/redux/store';
import NextLink from 'next/link';
import { useRouter } from 'next/router'

type UserType = {
    id: string;
    name: string;
    email: string;
    phone: string;
    password: string;
    confirmPassword: string;
    roles: string[];
};

let validName = (str: string) => {
    let pattern = /^[a-zA-Z\s]+$/;
    return pattern.test(str);
}

function CreateUser() {
    const dispatch = useDispatch();
    const router = useRouter()

    useEffect(() => {
        dispatch(getRoles())
        return () => {
            dispatch(clearStateRoles())
        }
    }, []);

    const rolesArray: any = useSelector((state: RootState) => state.roles.roles);
    const [rolesListed, setRolesListed] = useState(rolesArray)
    const [rolesAdd, setRolesAdd] = useState([])

    useEffect(() => {
        setRolesListed(rolesArray)
    }, [rolesArray]);

    const [userData, setUserData] = useState<UserType>({
        id: "",
        name: "",
        email: "",
        phone: "",
        password: "",
        confirmPassword: "",
        roles: [],
    });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setUserData({ ...userData, [e.target.name]: e.target.value });
    };

    const [charge, setCharge] = useState(false);

    const handleSubmit = (e: React.ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();
        if ((userData.password === userData.confirmPassword) && userData.password.length >= 6) {
            if (validName(userData.name)) {
                setCharge(true);
                  let uuid = uuidv4();
                  dispatch(createUser(uuid, userData.email, userData.name, userData.password, userData.phone, userData.roles))
                    .then(r => {
                      if (r === undefined) {
                        alert("Usuario creado con éxito")
                        setCharge(false);
                        router.push("/usuarios")
                      }
                      if (r && r.name === "AxiosError") {
                        alert("Ocurrió un error.")
                        setCharge(false);
                      }
                    })
            } else {
                if (!validName(userData.name)) alert("El nombre no debe contener caracteres especiales ni números.")
            }
        } else {
            if (userData.password.length < 6) alert("La contraseña debe tener al menos 6 caracteres.")
            else alert("Por favor, ingrese contraseñas iguales.")
        }
    };

    const handleSelectAddRoles = (event: React.ChangeEvent<HTMLSelectElement>) => {
        var rolArray = event.target.value.split("%")
        var rol = { id: rolArray[1], name: rolArray[0] }
        var filter = rolesListed.filter(p => p.id !== rol.id)
        setRolesAdd([...rolesAdd, rol])
        setRolesListed(filter)
        setUserData({
            ...userData,
            roles: [...userData.roles, rol.id]
        })
    };

    const handleSelectOutRoles = (event: React.ChangeEvent<HTMLSelectElement>) => {
        var rolArray = event.target.value.split("%")
        var rol = { id: rolArray[1], name: rolArray[0] }
        var filter = rolesAdd.filter(p => p.id !== rol.id)
        setRolesListed([rol, ...rolesListed])
        setRolesAdd(filter)
        setUserData({
            ...userData,
            roles: userData.roles.filter(p => p !== rol.id)
        })
    };

    return (
        <>
            <Head>
                <title>Crear usuario</title>
            </Head>
            <PageTitleWrapper>
                <Grid container justifyContent="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h3" component="h3" gutterBottom>
                            Crear usuario
                        </Typography>
                    </Grid>
                    <Grid item>
                        <NextLink href="/usuarios" passHref>
                            <Button
                                sx={{ mt: { xs: 2, md: 0 } }}
                                variant="contained"
                                startIcon={<PersonAddIcon fontSize="small" />}
                            >
                                Volver
                            </Button>
                        </NextLink>
                    </Grid>
                </Grid>
            </PageTitleWrapper>
            {rolesArray.length ?
            <Container maxWidth="lg">
                <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="stretch"
                    spacing={3}
                >
                    <Grid item xs={12}>
                        <Card>
                            <CardHeader title="Rellene los campos" />
                            <Divider />
                            <CardContent>
                                <Box
                                    component="form"
                                    onSubmit={handleSubmit}
                                    sx={{
                                        '& .MuiTextField-root': { m: 1, mb: 2, width: "100%", display: "flex" }
                                    }}
                                >
                                    <div>
                                        <TextField
                                            required
                                            id="standard-required"
                                            name="name"
                                            type="text"
                                            label="Nombre"
                                            variant="standard"
                                            onChange={handleChange}
                                        />
                                        <TextField
                                            required
                                            id="standard-required"
                                            name="email"
                                            type="email"
                                            label="Email"
                                            variant="standard"
                                            onChange={handleChange}
                                        />
                                        <TextField
                                            required
                                            id="standard-number"
                                            name="phone"
                                            type="number"
                                            label="Teléfono"
                                            variant="standard"
                                            onChange={handleChange}
                                        />
                                        <Typography sx={{ textAlign: "center", fontWeight: "bold" }}>Roles</Typography>
                                        <Box sx={{ display: "flex", mb: 3 }}>
                                            <Select
                                                fullWidth
                                                multiple
                                                native
                                                // @ts-ignore Typings are not considering `native`
                                                onChange={handleSelectAddRoles}
                                                inputProps={{
                                                    id: 'select-multiple-native',
                                                }}
                                            >
                                                {rolesListed.map((rol) => (
                                                    <option key={rol.id} value={`${rol.name}%${rol.id}`}>
                                                        {rol.name}
                                                    </option>
                                                ))}
                                            </Select>
                                            <Select
                                                fullWidth
                                                multiple
                                                native
                                                // @ts-ignore Typings are not considering `native`
                                                onChange={handleSelectOutRoles}
                                                inputProps={{
                                                    id: 'select-multiple-native',
                                                }}
                                            >
                                                {rolesAdd.map((rol) => (
                                                    <option key={rol.id} value={`${rol.name}%${rol.id}`}>
                                                        {rol.name}
                                                    </option>
                                                ))}
                                            </Select>
                                        </Box>
                                        <TextField
                                            required
                                            id="standard-password-input"
                                            name="password"
                                            type="password"
                                            label="Contraseña"
                                            variant="standard"
                                            onChange={handleChange}
                                        />
                                        <TextField
                                            required
                                            id="standard-password-input"
                                            name="confirmPassword"
                                            type="password"
                                            label="Confirmar contraseña"
                                            variant="standard"
                                            onChange={handleChange}
                                        />
                                        <br />
                                        <Divider />
                                        <br />
                                        <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
                                            {!charge ? null : <CircularProgress color="success" sx={{ mr: 5 }} />}
                                            <Button
                                                startIcon={<SaveIcon fontSize="small" />}
                                                type="submit"
                                                variant="contained"
                                                sx={{ mr: 2, width: "100px", backgroundColor: "#00C412", "&:hover": { backgroundColor: "#087500" } }}
                                            >
                                                Añadir
                                            </Button>
                                            <NextLink href="/usuarios" passHref>
                                                <Button
                                                    startIcon={<CancelIcon fontSize="small" />}
                                                    variant="contained"
                                                    sx={{ mr: 2, width: "100px", backgroundColor: "red", "&:hover": { backgroundColor: "#9B0000" } }}
                                                >
                                                    Cancelar
                                                </Button>
                                            </NextLink>
                                        </Box>
                                    </div>
                                </Box>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </Container>  :
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <CircularProgress />
          </Box>
        }
            <Footer />
        </>
    );
}


CreateUser.getLayout = (page) => (
    <SidebarLayout>{page}</SidebarLayout>
);

export default CreateUser;
