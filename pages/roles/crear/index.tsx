import Head from 'next/head';
import SidebarLayout from '@/layouts/SidebarLayout';
import { useState, useEffect } from 'react';
import PageTitleWrapper from 'src/components/PageTitleWrapper';
import {
  Container,
  Grid,
  Card,
  CardHeader,
  CardContent,
  Divider,
  Typography,
  Button,
  Select
} from '@mui/material';
import Footer from 'src/components/Footer';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Cancel';
import { useDispatch, useSelector } from 'react-redux';
import { clearStatePermissions, getPermissions } from 'src/redux/actions/permissionsAction';
import { RootState } from 'src/redux/store';
import CircularProgress from '@mui/material/CircularProgress';
import NextLink from 'next/link';
import { v4 as uuidv4 } from 'uuid';
import { createRoles } from 'src/redux/actions/rolesAction';
import { useRouter } from 'next/router';

type RolesType = {
  id: string;
  name: string;
  slug: string;
  description: string;
  permissions: string[];
  status: string;
};

const currencies = [
  {
    value: 'on',
    label: 'Activo'
  },
  {
    value: 'off',
    label: 'Inactivo'
  }
];

let validName = (str: string) => {
  let pattern = /^[A-Za-z0-9_.]+$/;
  return pattern.test(str);
}

function CreateRoles() {
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    dispatch(getPermissions())
    return () => {
      dispatch(clearStatePermissions())
    }
  }, []);

  const permissionsArray: any = useSelector((state: RootState) => state.permissions.permissions);
  const [permissionsListed, setPermissionsListed] = useState(permissionsArray)
  const [permissionsAdd, setPermissionsAdd] = useState([])

  useEffect(() => {
    setPermissionsListed(permissionsArray)
  }, [permissionsArray]);

  const [rolesData, setRolesData] = useState<RolesType>({
    id: "",
    name: "",
    slug: "",
    description: "",
    permissions: [],
    status: "on",
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setRolesData({ ...rolesData, [e.target.name]: e.target.value });
  };

  const [charge, setCharge] = useState(false);

  const handleSubmit = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    if (validName(rolesData.name) && validName(rolesData.slug) && rolesData.permissions.length) {
      setCharge(true);
      let uuid = uuidv4();
      dispatch(createRoles(uuid, rolesData.name, rolesData.slug, rolesData.description, rolesData.permissions, rolesData.status))
        .then(r => {
          if (r === undefined) {
            alert("Rol creado con éxito")
            setCharge(false);
            router.push("/roles")
          }
          if (r && r.name === "AxiosError") {
            alert("Ocurrió un error.")
            setCharge(false);
          }
        })
    } else {
      if (!rolesData.permissions.length) {
        alert("Por favor, ingrese al menos un permiso.")
      }
      if (!validName(rolesData.name) && !validName(rolesData.slug)) {
        alert("El nombre y slug no deben contener caractéres especiales.")
      }
    }
  };

  const handleSelectAddPermission = (event: React.ChangeEvent<HTMLSelectElement>) => {
    var permissionArray = event.target.value.split("%")
    var permission = { id: permissionArray[1], name: permissionArray[0] }
    var filter = permissionsListed.filter(p => p.id !== permission.id)
    setPermissionsAdd([...permissionsAdd, permission])
    setPermissionsListed(filter)
    setRolesData({
      ...rolesData,
      permissions: [...rolesData.permissions, permission.id]
    })
  };

  const handleSelectOutPermission = (event: React.ChangeEvent<HTMLSelectElement>) => {
    var permissionArray = event.target.value.split("%")
    var permission = { id: permissionArray[1], name: permissionArray[0] }
    var filter = permissionsAdd.filter(p => p.id !== permission.id)
    setPermissionsListed([permission, ...permissionsListed])
    setPermissionsAdd(filter)
    setRolesData({
      ...rolesData,
      permissions: rolesData.permissions.filter(p => p !== permission.id)
    })
  };

  return (
    <>
      <Head>
        <title>Crear rol</title>
      </Head>
      <PageTitleWrapper>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h3" component="h3" gutterBottom>
              Crear rol
            </Typography>
          </Grid>
          <Grid item>
            <NextLink href="/roles" passHref>
              <Button
                sx={{ mt: { xs: 2, md: 0 } }}
                variant="contained"
                startIcon={<PersonAddIcon fontSize="small" />}
              >
                Volver
              </Button>
            </NextLink>
          </Grid>
        </Grid>
      </PageTitleWrapper>
      {permissionsArray.length ?
        <Container maxWidth="lg">
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="stretch"
            spacing={3}
          >
            <Grid item xs={12}>
              <Card>
                <CardHeader title="Rellene los campos" />
                <Divider />
                <CardContent>
                  <Box
                    component="form"
                    onSubmit={handleSubmit}
                    sx={{
                      '& .MuiTextField-root': { m: 1, mb: 2, width: "100%", display: "flex" }
                    }}
                  >
                    <div>
                      <TextField
                        required
                        id="standard-required"
                        name="name"
                        type="text"
                        label="Nombre"
                        variant="standard"
                        onChange={handleChange}
                      />
                      <TextField
                        required
                        id="standard-required"
                        name="slug"
                        type="text"
                        label="Slug"
                        variant="standard"
                        onChange={handleChange}
                      />
                      <TextField
                        required
                        id="standard-required"
                        name="description"
                        type="text"
                        label="Descripción"
                        variant="standard"
                        onChange={handleChange}
                      />
                      <Typography sx={{ textAlign: "center", fontWeight: "bold" }}>Permisos</Typography>
                      <Box sx={{ display: "flex", mb: 3 }}>
                        <Select
                          fullWidth
                          multiple
                          native
                          // @ts-ignore Typings are not considering `native`
                          onChange={handleSelectAddPermission}
                          inputProps={{
                            id: 'select-multiple-native',
                          }}
                        >
                          {permissionsListed.map((permission) => (
                            <option key={permission.id} value={`${permission.name}%${permission.id}`}>
                              {permission.name}
                            </option>
                          ))}
                        </Select>
                        <Select
                          fullWidth
                          multiple
                          native
                          // @ts-ignore Typings are not considering `native`
                          onChange={handleSelectOutPermission}
                          inputProps={{
                            id: 'select-multiple-native',
                          }}
                        >
                          {permissionsAdd.map((permission) => (
                            <option key={permission.id} value={`${permission.name}%${permission.id}`}>
                              {permission.name}
                            </option>
                          ))}
                        </Select>
                      </Box>

                      <TextField
                        required
                        id="standard-select-currency-native"
                        select
                        name="status"
                        label="Estado"
                        onChange={handleChange}
                        SelectProps={{
                          native: true
                        }}
                        variant="standard"
                      >
                        {currencies.map((option) => (
                          <option key={option.value} value={option.value}>
                            {option.label}
                          </option>
                        ))}
                      </TextField>
                      <br />
                      <Divider />
                      <br />
                      <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
                        {!charge ? null : <CircularProgress color="success" sx={{ mr: 5 }} />}
                        <Button
                          startIcon={<SaveIcon fontSize="small" />}
                          type="submit"
                          variant="contained"
                          sx={{ mr: 2, width: "100px", backgroundColor: "#00C412", "&:hover": { backgroundColor: "#087500" } }}
                        >
                          Añadir
                        </Button>
                        <NextLink href="/roles" passHref>
                          <Button
                            startIcon={<CancelIcon fontSize="small" />}
                            variant="contained"
                            sx={{ mr: 2, width: "100px", backgroundColor: "red", "&:hover": { backgroundColor: "#9B0000" } }}
                          >
                            Cancelar
                          </Button>
                        </NextLink>
                      </Box>
                    </div>
                  </Box>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Container>
        :
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <CircularProgress />
        </Box>
      }
      <Footer />
    </>
  );
}

CreateRoles.getLayout = (page) => (
  <SidebarLayout>{page}</SidebarLayout>
);

export default CreateRoles;
